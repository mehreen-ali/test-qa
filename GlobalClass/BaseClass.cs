﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class3automation.GlobalClass
{
    class BaseClass
    {
      string link = "http://www.uitestpractice.com/";
      public IWebDriver driver = new ChromeDriver();
       public WebDriverWait wait;
        public void VerifyForm(string URL,string Title ,string heading)
        {
            driver.Manage().Window.Maximize();
            if (driver.Url.Equals("http://www.uitestpractice.com/"))
            {
                Assert.AreEqual(driver.Url, URL, "url doesnot Match");
                Assert.AreEqual(driver.Title, Title, "tITLE DOESNT MATCH");
                Assert.AreEqual(driver.PageSource.Contains(heading), "Heading doesnot exits");

            }
            else
            {
                driver.Navigate().GoToUrl(link + URL);
                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
                wait.Until(driver => driver.FindElement(By.CssSelector("body > div.navbar.navbar-inverse.navbar-fixed-top > div > div.navbar-header > a")).Displayed);
                Assert.AreEqual(driver.Url, link + URL, "Url Match");
                Assert.AreEqual(driver.Title, Title, "Title Match");
                Assert.AreEqual(driver.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(1) > form > div > span")).Text,heading,"Heading match");
               // Assert.AreEqual(driver.FindElement(By.CssSelector("body > app-root > app-home > div > div.container-fluid.fntFmly > div:nth-child(2) > div.col-lg-11.col-md-11.col-sm-11.col-xs-12 > h3")).Text, heading, "Heading Does not Match"); // Update the header Path due to Rehan new work .

            }
        }
    }
}
