﻿using Class3automation.GlobalClass;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Class3automation.Application.Form.Google.Scenrio_Test_cases
{
    class Method_cs : BaseClass
    {

        // IWebDriver driver = new ChromeDriver();

        public void OpenBrowser()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.google.com/");
        }

        public void DML()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://www.uitestpractice.com/Students/Form");
            driver.FindElement(By.Id("firstname")).SendKeys("Mehreen");
            driver.FindElement(By.Id("lastname")).SendKeys("Ali");
            driver.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(1) > div > form > div:nth-child(3) > label:nth-child(3)")).Click();
            driver.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(1) > div > form > div:nth-child(5) > label:nth-child(4)")).Click();
            SelectElement s1 = new SelectElement(driver.FindElement(By.CssSelector("#sel1")));
            s1.SelectByText("Canada");
            driver.FindElement(By.CssSelector("#datepicker")).Click();
            driver.FindElement(By.CssSelector("#ui-datepicker-div > table > tbody > tr:nth-child(4) > td:nth-child(2) > a")).Click();
            driver.FindElement(By.XPath("//*[@id='phonenumber']")).SendKeys("923458555888");
            driver.FindElement(By.CssSelector("#username")).SendKeys("mehreen_ali");
            driver.FindElement(By.CssSelector("#email")).SendKeys("mehreen@gmail.com");
            driver.FindElement(By.CssSelector("#comment")).SendKeys("i am in automation class");
            driver.FindElement(By.CssSelector("#pwd")).SendKeys("mehreen123");
            Thread.Sleep(3000);
            driver.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(1) > div > form > div:nth-child(20) > div > button")).Click();
            /////////////// driver.FindElement/////////////////9
        }
        public void serch()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://www.uitestpractice.com/Students/Index");
            driver.FindElement(By.CssSelector("#Search_Data")).SendKeys("Mehreen");
            driver.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(1) > form > div > span > input.btn")).Click();
            string A = driver.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(1) > table > tbody > tr:nth-child(2) > td:nth-child(1)")).Text;
            Console.WriteLine(A);
            //MA
            Assert.AreEqual(A, "mehreen");

        }
        public void GridConcept()
        {

            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://www.uitestpractice.com/Students/Index");
            String VAL = driver.FindElement(By.CssSelector("body > div.container.body-content > div:nth-child(1) > table > tbody > tr:nth-child(5)")).Text;
            Console.WriteLine(VAL);
            Assert.IsTrue(true);

        }
        public void dATEPICKERDD()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://www.uitestpractice.com/Students/Form");
            driver.FindElement(By.CssSelector("#datepicker")).Click();
            SelectElement oselect = new SelectElement(driver.FindElement(By.CssSelector("#ui-datepicker-div > div > div > select.ui-datepicker-month")));
            oselect.SelectByValue("1");
            SelectElement newsel = new SelectElement(driver.FindElement(By.CssSelector("#ui-datepicker-div > div > div > select.ui-datepicker-year")));
            newsel.SelectByValue("2002");
            driver.FindElement(By.CssSelector("#ui-datepicker-div > table > tbody > tr:nth-child(4) > td:nth-child(5) > a")).Click();

        }
        public void dATEPICKERDob()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://www.uitestpractice.com/Students/Form");
            Thread.Sleep(5000);
            driver.FindElement(By.CssSelector("#datepicker")).Click();
            for(int i=1;i<=10; i++)
            {
                driver.FindElement(By.CssSelector("#ui-datepicker-div > div > a.ui-datepicker-prev.ui-corner-all > span")).Click();
            }
            driver.FindElement(By.CssSelector("#ui-datepicker-div > table > tbody > tr:nth-child(4) > td:nth-child(5) > a")).Click();

        }
    }
}